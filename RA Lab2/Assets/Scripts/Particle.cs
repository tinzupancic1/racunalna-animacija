
using System;
using UnityEngine;

public class Particle : MonoBehaviour
{
    public float lifetime;
    public float time;
    public Vector3 speed;

    private SpriteRenderer _renderer;

    public SpriteRenderer Renderer
    {
        get
        {
            if (_renderer != null)
                return _renderer;
            _renderer = GetComponent<SpriteRenderer>();
            if (_renderer == null)
                _renderer = gameObject.AddComponent<SpriteRenderer>();
            return _renderer;
        }
    }

    public bool IsDead => time >= lifetime;

    public void Orient()
    {
        var cam = Camera.main;
        if (cam == null)
            return;
        var camPos = cam.transform.position;
        transform.rotation = Quaternion.LookRotation(camPos - transform.position);
    }
}
