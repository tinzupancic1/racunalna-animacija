using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public static class ObjectPool<T> where T : Component
{
    private static Queue<T> _pool = new Queue<T>();

    public static T Get()
    {
        while (_pool.Count < 5) 
            Put(new GameObject(typeof(T).Name).AddComponent<T>());
        return _pool.Dequeue();
    }

    public static void Put(T obj)
    {
        obj.gameObject.SetActive(false);
        _pool.Enqueue(obj);
    }
}
