using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class BSpline
{
    private List<Vector3> _points = new List<Vector3>();
    
    public Vector3 Position(float t)
    {
        var b = new Matrix4x4();
        b.SetRow(0, new Vector4(-1, 3, -3, 1));
        b.SetRow(1, new Vector4(3, -6, 3, 0));
        b.SetRow(2, new Vector4(-3, 0, 3, 0));
        b.SetRow(3, new Vector4(1, 4, 1, 0));

        int i = (int) t;
        t -= i;
        var r = new Matrix4x4();
        r.SetRow(0, _points[i]);
        r.SetRow(1, _points[i + 1]);
        r.SetRow(2, _points[i + 2]);
        r.SetRow(3, _points[i + 3]);
        
        var tv = new Vector4(t * t * t, t * t, t, 1);

        return (b * r).transpose * tv / 6;
    }
    
    public Vector3 Tangent(float t)
    {
        var b = new Matrix4x4();
        b.SetRow(0, new Vector4(-1, 3, -3, 1));
        b.SetRow(1, new Vector4(2, -4, 2, 0));
        b.SetRow(2, new Vector4(-1, 0, 1, 0));

        int i = (int) t;
        t -= i;
        var r = new Matrix4x4();
        r.SetRow(0, _points[i]);
        r.SetRow(1, _points[i + 1]);
        r.SetRow(2, _points[i + 2]);
        r.SetRow(3, _points[i + 3]);
        
        var tv = new Vector3(t * t, t, 1);

        return (b * r).transpose * tv * 0.5f;
    }

    public float Length => _points.Count - 3;

    public static BSpline Load(string file)
    {
        BSpline bs = new BSpline();
        var reader = new StreamReader(file);
        while (!reader.EndOfStream)
        {
            var line = reader.ReadLine();
            if (line == null)
                continue;
            var parts = line.Split();
            bs._points.Add(new Vector3( float.Parse(parts[1]), float.Parse(parts[2]), float.Parse(parts[3])));
        }

        return bs;
    }
}
