using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;


public class CustomParticleSystem : MonoBehaviour
{
    [SerializeField] private Sprite _sprite;
    [SerializeField] [Range(0, 20)] private float spawnRate;
    [SerializeField] private float lifetimeMin;
    [SerializeField] private float lifetimeMax;
    [SerializeField] private Gradient colorOverLifetime;
    [SerializeField] private AnimationCurve sizeOverLifetime;
    
    private List<Particle> _particles = new List<Particle>();

    private float _spawnPending;

    private void Spawn()
    {
        _spawnPending += Random.Range(0, spawnRate) * Time.deltaTime;
        for (int i = 1; i < _spawnPending; ++i)
        {
            var particle = ObjectPool<Particle>.Get();
            particle.gameObject.SetActive(true);
            particle.Renderer.sprite = _sprite;
            particle.lifetime = Random.Range(lifetimeMin, lifetimeMax);
            particle.time = 0;
            particle.speed = Random.insideUnitSphere.normalized * Random.Range(0.1f, 1f);
            _particles.Add(particle);
        }

        _spawnPending -= Mathf.Floor(_spawnPending);
    }

    private void Update()
    {
        Spawn();
        List<Particle> toRemove = new List<Particle>();
        foreach (var particle in _particles)
        {
            particle.time += Time.deltaTime;
            if (particle.IsDead)
            {
                toRemove.Add(particle);
                continue;
            }
            
            particle.transform.position = transform.position + particle.speed * particle.time;
            particle.Renderer.color = colorOverLifetime.Evaluate(particle.time / particle.lifetime);
            particle.transform.localScale = Vector3.one * sizeOverLifetime.Evaluate(particle.time / particle.lifetime);
            particle.Orient();
        }
        
        toRemove.ForEach(x =>
        {
            _particles.Remove(x);
            ObjectPool<Particle>.Put(x);
        });
    }
}
