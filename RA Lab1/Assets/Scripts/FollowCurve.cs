using System;
using UnityEngine;

public class FollowCurve : MonoBehaviour
{
    [SerializeField] private BSplineRenderer _bSpline;
    [SerializeField] private Vector3 offset;

    private float _t;
    private Vector3 _forward;

    private void Start()
    {
        _forward = transform.forward;
    }

    private void Update()
    {
        _t -= 0.2f * Time.deltaTime;
        if (_t > _bSpline.Curve.Length)
            _t -= _bSpline.Curve.Length;
        if (_t < 0)
            _t += _bSpline.Curve.Length;
        var position = _bSpline.Curve.Position(_t);
        var tangent = _bSpline.Curve.Tangent(_t);

        transform.position = position;

        // var s = transform.forward;
        var s = _forward;
        var e = tangent.normalized;

        var axis = Vector3.Cross(s, e);
        var angle = Mathf.Acos(Vector3.Dot(s, e));

        // transform.rotation *= Quaternion.AngleAxis(angle * Mathf.Rad2Deg, axis);
        transform.rotation = Quaternion.AngleAxis(angle * Mathf.Rad2Deg, axis);
    }
}
