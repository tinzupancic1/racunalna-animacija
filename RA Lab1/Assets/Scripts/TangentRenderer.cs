using UnityEngine;

namespace UnityTemplateProjects
{
    public class TangentRenderer : MonoBehaviour
    {
        [SerializeField] private BSplineRenderer _bSpline;
        
        private LineRenderer _renderer;
        private float _t;
        
        private void Start()
        {
            _renderer = GetComponent<LineRenderer>();
            if (_renderer == null)
                return;
            _renderer.positionCount = 2;
            _renderer.useWorldSpace = false;
        }
        
        private void Update()
        {
            if (_renderer == null)
                return;

            _t += 0.2f * Time.deltaTime;
            if (_t > _bSpline.Curve.Length)
                _t -= _bSpline.Curve.Length;
            var position = _bSpline.Curve.Position(_t);
            var tangent = _bSpline.Curve.Tangent(_t);

            _renderer.SetPosition(0, position - 0.5f * tangent);
            _renderer.SetPosition(1, position + 0.5f * tangent);
        }
    }
}