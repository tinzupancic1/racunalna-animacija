using System;
using System.Collections.Generic;
using UnityEngine;

public class BSplineRenderer : MonoBehaviour
{
    [SerializeField] private string _file;
    private BSpline _bSpline;
    private LineRenderer _curveRenderer;

    public BSpline Curve => _bSpline;
    
    private void Start()
    {
        _bSpline = BSpline.Load(_file);
        _curveRenderer = GetComponent<LineRenderer>();
        if (_curveRenderer == null)
            _curveRenderer = gameObject.AddComponent<LineRenderer>();

        _curveRenderer.useWorldSpace = false;

        List<Vector3> positions = new List<Vector3>();
        for (float t = 0; t < _bSpline.Length; t += 0.1f)
            positions.Add(_bSpline.Position(t));

        _curveRenderer.positionCount = positions.Count;
        _curveRenderer.SetPositions(positions.ToArray());
    }
}